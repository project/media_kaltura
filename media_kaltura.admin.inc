<?php

/**
 * @file
 * Admin UI functionality for the media_kaltura module.
 */

/**
 * Returns the media_kaltura admin form.
 */
function media_kaltura_admin_form($form, $form_state) {
  $settings = media_kaltura_get_settings();

  // Check that all libraries exist.
  $required_libraries = array('KalturaClient');
  foreach ($required_libraries as $name) {
    $library = libraries_detect($name);
    if (!$library['installed']) {
      drupal_set_message($library['error message'], 'error');
    }
  }

  // Load existing servers and add to table options array.
  $rows = array();
  $options = array();
  $servers = entity_load('media_kaltura_server');
  foreach ($servers as $server) {
    $rows[$server->id] = array(
      'domain' => $server->domain,
      'force_ssl' => $server->force_ssl ? 'Yes' : 'No',
      'api' => $server->api ? 'Enabled' : 'Disabled',
      'partner_id' => $server->partner_id,
      'subpartner_id' => $server->subpartner_id,
      'user_id' => $server->user_id,
      'secret' => $server->secret,
      'uiconf_id' => $server->uiconf_id,
      'operations' => l(t('edit'), 'admin/config/media/media_kaltura/server/' . $server->id),
    );
    $options[$server->id] = $server->domain;
  }

  // Enabled Kaltura servers list.
  $form['media_kaltura'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
    'servers' => array(
      '#type' => 'fieldset',
      '#title' => t('Kaltura Servers'),
      'list' => array(
        '#theme' => 'table',
        '#header' => array(
          'domain' => t('Domain'),
          'force_ssl' => t('Force SSL'),
          'api' => t('Kaltura Client'),
          'partner_id' => t('Partner ID'),
          'subpartner_id' => t('Sub Partner ID'),
          'user_id' => t('User ID'),
          'secret' => t('User Secret'),
          'uiconf_id' => t('UI Conf ID'),
          'operations' => t('Operations'),
        ),
        '#rows' => $rows,
        '#empty' => t('No servers added yet.'),
      ),
    ),
  );

  // Entry creation settings.
  $form['media_kaltura']['create'] = array(
    '#type' => 'fieldset',
    '#title' => t('Media Entry Creation Settings'),
    'enable' => array(
      '#type' => 'checkbox',
      '#title' => t('Enable upload and media entry creation functionality?'),
      '#description' => t('This will allow Drupal to upload files directly to a Kaltura server. This will enable a field widget and form api element.'),
      '#default_value' => $settings['create']['enable'],
    ),
    'server' => array(
      '#type' => 'select',
      '#title' => t('Default server to upload files to?'),
      '#description' => t('Files will be automatically uploaded to the selected Kaltura server.'),
      '#default_value' => $settings['create']['server'],
      '#options' => $options,
      '#states' => array(
        'disabled' => array(
          ':input[name="media_kaltura[create][enable]"]' => array('checked' => FALSE),
        ),
      ),
    ),
    'browser' => array(
      '#type' => 'checkbox',
      '#title' => t('Enable the Kaltura Media Browser tab?'),
      '#description' => t('This will enable client side upload to a Kaltura server in the Media Browser and file/add/kaltura page.'),
      '#default_value' => $settings['create']['browser'],
      '#states' => array(
        'disabled' => array(
          ':input[name="media_kaltura[create][enable]"]' => array('checked' => FALSE),
        ),
      ),
    ),
    'upload' => array(
      '#type' => 'checkbox',
      '#title' => t('Automatically upload files to Kaltura server if they match an allowed mimetype?'),
      '#description' => t('All local files will be automatically uploaded to the Kaltura server if they match one of the allowed mimetypes below.<br /><br /><strong>Note: This will trigger on all file save operations. Be sure this is the expected outcome before enabling this option!</strong>'),
      '#default_value' => $settings['create']['upload'],
      '#states' => array(
        'disabled' => array(
          ':input[name="media_kaltura[create][enable]"]' => array('checked' => FALSE),
        ),
      ),
    ),
    'mimetypes' => array(
      '#type' => 'textarea',
      '#title' => t('Allowed mimetypes'),
      '#description' => t('Each mimetype should be separated by a new line. If you add mimetypes make sure they are supported by your Kaltura server.'),
      '#default_value' => implode("\n", $settings['create']['mimetypes']),
      '#states' => array(
        'disabled' => array(
          ':input[name="media_kaltura[create][enable]"]' => array('checked' => FALSE),
        ),
      ),
    ),
    'reference' => array(
      '#type' => 'checkbox',
      '#title' => t('Automatically add the file ID to each media entry reference field?'),
      '#description' => t('File fid will be added to the Kaltura media entry referenceId field when uploading or updating managed files.'),
      '#default_value' => $settings['create']['reference'],
      '#states' => array(
        'disabled' => array(
          ':input[name="media_kaltura[create][enable]"]' => array('checked' => FALSE),
        ),
      ),
    ),
  );

  // Entry edit settings.
  $form['media_kaltura']['edit'] = array(
    '#type' => 'fieldset',
    '#title' => t('Media Entry Edit Settings'),
    'enable' => array(
      '#type' => 'checkbox',
      '#title' => t('Include Kaltura media entry fields on the file edit form?'),
      '#description' => t('Note that one of the options below must be selected for this to affect anything.'),
      '#default_value' => $settings['edit']['enable'],
    ),
    'filename' => array(
      '#type' => 'checkbox',
      '#title' => t('Sync the media entry title with the filename field?'),
      '#default_value' => $settings['edit']['filename'],
      '#states' => array(
        'disabled' => array(
          ':input[name="media_kaltura[edit][enable]"]' => array('checked' => FALSE),
        ),
      ),
    ),
    'description' => array(
      '#type' => 'checkbox',
      '#title' => t('Add the media entry description field to the file edit form?'),
      '#default_value' => $settings['edit']['description'],
      '#states' => array(
        'disabled' => array(
          ':input[name="media_kaltura[edit][enable]"]' => array('checked' => FALSE),
        ),
      ),
    ),
    'categories' => array(
      '#type' => 'checkbox',
      '#title' => t('Add the media entry categories field to the file edit form?'),
      '#default_value' => $settings['edit']['categories'],
      '#states' => array(
        'disabled' => array(
          ':input[name="media_kaltura[edit][enable]"]' => array('checked' => FALSE),
        ),
      ),
    ),
  );

  // Entry delete settings.
  $form['media_kaltura']['delete'] = array(
    '#type' => 'fieldset',
    '#title' => t('Media Entry Delete Settings'),
    'enable' => array(
      '#type' => 'checkbox',
      '#title' => t('Delete the corresponding Kaltura media entry when deleting the Drupal managed file?'),
      '#description' => t('This will delete the media entry and file on the Kaltura server when the managed file is deleted on your Drupal site.'),
      '#default_value' => $settings['delete']['enable'],
    ),
  );

  // Entry create settings: Add transcoding profile and category default
  // elements if a default server has been selected.
  if (isset($settings['create']['enable']) && $settings['create']['enable'] && isset($settings['create']['server']) && $settings['create']['server']) {

    // Attempt to start a Kaltura session.
    try {
      $server = media_kaltura_server_load($settings['create']['server']);
      if (!$server) {
        throw new Exception('Unable to load Kaltura server.');
      }
      $kaltura = media_kaltura_start_session($server);
      if (!$kaltura) {
        throw new Exception('Unable to start Kaltura session.');
      }

      // Default transcoding profile.
      $filter = new KalturaConversionProfileFilter();
      $results = $kaltura['client']->conversionProfile->listAction($filter);
      $profiles = array('-- No Default --');
      foreach ($results->objects as $result) {
        $profiles[$result->id] = $result->name;
      }
      $form['media_kaltura']['create']['profile'] = array(
        '#type' => 'select',
        '#title' => t('Select a Default Transcoding Profile'),
        '#description' => t('All new media entries will automatically be transcoded using this transcoding profile.'),
        '#options' => $profiles,
        '#default_value' => isset($settings['create']['profile']) ? $settings['create']['profile'] : 0,
        '#states' => array(
          'disabled' => array(
            ':input[name="media_kaltura[create][enable]"]' => array('checked' => FALSE),
          ),
        ),
      );

      // Default category.
      $filter = new KalturaCategoryFilter();
      $results = $kaltura['client']->category->listAction($filter);
      $categories = array('-- No Default --');
      foreach ($results->objects as $result) {
        $categories[$result->id] = $result->fullName;
      }
      $form['media_kaltura']['create']['category'] = array(
        '#type' => 'select',
        '#title' => t('Select a Default Category'),
        '#description' => t('All new media entries will be placed within this default category.'),
        '#options' => $categories,
        '#default_value' => isset($settings['create']['category']) ? $settings['create']['category'] : 0,
        '#states' => array(
          'disabled' => array(
            ':input[name="media_kaltura[create][enable]"]' => array('checked' => FALSE),
          ),
        ),
      );
    }
    catch (Exception $e) {
      watchdog('media_kaltura', 'There was a problem connecting to the kaltura server: @error', array('@error' => $e->getMessage()), WATCHDOG_ERROR);
    }
  }

  // Use system to set setting values.
  $form = system_settings_form($form);

  // Prepend extra submit handler so altering values is reflected when system
  // handler sets variables.
  array_unshift($form['#submit'], 'media_kaltura_admin_form_submit');

  // Clear cache after all submit handlers have run so menu, field, and element
  // items reflect current settings.
  $form['#submit'][] = 'drupal_flush_all_caches';

  $form['actions']['reset'] = array(
    '#type' => 'submit',
    '#value' => 'Reset mimetypes to default',
    '#submit' => array('media_kaltura_admin_form_reset'),
  );

  return $form;
}

/**
 * Validate handler for media_kaltura_admin_form().
 */
function media_kaltura_admin_form_validate($form, &$form_state) {
  if ($form_state['values']['media_kaltura']['create']['enable']) {

    $server = media_kaltura_server_load($form_state['values']['media_kaltura']['create']['server']);
    if (!$server) {
      form_set_error('media_kaltura][general][default', t('Unable to load server. Please check that your server information is correct.'));
    }

    // Start a new session with the Kaltura server.
    $kaltura = media_kaltura_start_session($server);
    if (!$kaltura) {
      form_set_error('media_kaltura][general][default', t('Unable to connect to server. Please check that your server information is correct.'));
    }
  }
}

/**
 * Submit handler for media_kaltura_admin_form().
 */
function media_kaltura_admin_form_submit($form, &$form_state) {
  $settings = media_kaltura_get_settings();

  // Process mimetype setting.
  if (!empty($form_state['values']['media_kaltura']['create']['mimetypes'])) {
    $form_state['values']['media_kaltura']['create']['mimetypes'] = preg_split('/[\r\n]+/', $form_state['values']['media_kaltura']['create']['mimetypes']);
  }
  elseif (!empty($settings['create']['mimetypes'])) {
    $form_state['values']['media_kaltura']['create']['mimetypes'] = $settings['create']['mimetypes'];
  }
  else {
    $form_state['values']['media_kaltura']['create']['mimetypes'] = media_kaltura_get_mimetypes();
  }
}

/**
 * Custom submit handler for media_kaltura_admin_form().
 */
function media_kaltura_admin_form_reset($form, &$form_state) {
  $mimetypes = media_kaltura_get_mimetypes();
  $settings = media_kaltura_get_settings();
  $settings['create']['mimetypes'] = $mimetypes;
  variable_set('media_kaltura', $settings);
}

/**
 * Returns the edit form for a kaltura server.
 */
function media_kaltura_server_edit_form($form, &$form_state, $id = 0) {
  $server = $id ? media_kaltura_server_load($id) : (object) array(
    'id' => 'new',
    'domain' => '',
    'force_ssl' => FALSE,
    'api' => FALSE,
    'api_url' => '',
    'partner_id' => '',
    'subpartner_id' => '',
    'user_id' => '',
    'secret' => '',
    'uiconf_id' => '',
  );

  $form['server'] = array(
    '#type' => 'container',
  );
  $form['server']['id'] = array(
    '#type' => 'hidden',
    '#value' => $server->id,
  );
  $form['server']['domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain'),
    '#required' => TRUE,
    '#description' => t('The domain of the kaltura server. For example, use cdnapi.kaltura.com for the commercial version of Kaltura.'),
    '#default_value' => $server->domain,
  );
  $form['server']['force_ssl'] = array(
    '#type' => 'checkbox',
    '#title' => t('Force SSL for this server?'),
    '#default_value' => $server->force_ssl,
  );
  $form['server']['api'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Kaltura Client for this server?'),
    '#description' => t('This will enable closer integration between the drupal managed file system and the Kaltura server. When a kaltura managed file is edited or deleted, the corresponding media entry on the Kaltura server will also be edited or deleted.'),
    '#default_value' => $server->api,
  );
  $form['server']['api_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Kaltura Client API Settings'),
    '#states' => array(
      'disabled' => array(
        ':input[name="api"]' => array('checked' => FALSE),
      ),
    ),
  );
  $form['server']['api_settings']['api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('API url'),
    '#description' => t('The API url to use when making api calls to the server. For Kaltura this should be http(s)://www.kaltura.com/api_v3/.'),
    '#default_value' => $server->api_url,
  );
  $form['server']['api_settings']['partner_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Partner ID'),
    '#description' => t('The partner ID of the account used to authenticate sessions with the Kaltura server.'),
    '#default_value' => $server->partner_id,
  );
  $form['server']['api_settings']['subpartner_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Sub Partner ID'),
    '#description' => t('The sub-partner ID of the account used to authenticate sessions with the Kaltura server.'),
    '#default_value' => $server->subpartner_id,
  );
  $form['server']['api_settings']['user_id'] = array(
    '#type' => 'textfield',
    '#title' => t('User ID'),
    '#description' => t('The ID of the user account used to authenticate sessions with the Kaltura server.'),
    '#default_value' => $server->user_id,
  );
  $form['server']['api_settings']['secret'] = array(
    '#type' => 'textfield',
    '#title' => t('User Secret Key'),
    '#description' => t('The secret key of the user account used to authenticate sessions with the Kaltura server.'),
    '#default_value' => $server->secret,
  );
  $form['server']['api_settings']['uiconf_id'] = array(
    '#type' => 'textfield',
    '#title' => t('UI Conf ID'),
    '#description' => t('The id of the player used to play videos from this account.'),
    '#default_value' => $server->uiconf_id,
  );
  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save Server'),
    ),
  );
  if (!empty($server->domain)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('media_kaltura_server_delete_form_submit'),
    );
  }

  return $form;
}

/**
 * Validate handler for media_kaltura_server_edit_form().
 */
function media_kaltura_server_edit_form_validate($form, &$form_state) {
  $server = (object) $form_state['values'];

  // Attempt to start session with server information if API is enabled.
  if ($server->api) {
    if (!media_kaltura_start_session($server)) {
      form_set_error('server', t('Unable to connect to server. Please check that your server information is correct.'));
    }
  }
}

/**
 * Submit handler for media_kaltura_server_edit_form().
 */
function media_kaltura_server_edit_form_submit($form, &$form_state) {
  $server = (object) $form_state['values'];

  // Save new server.
  if ($server->id === 'new') {
    drupal_write_record('media_kaltura_server', $server);
    drupal_set_message(t('New server successfully added.'));
  }

  // Update server.
  elseif (is_numeric($server->id)) {
    drupal_write_record('media_kaltura_server', $server, 'id');
    drupal_set_message(t('Server successfully updated.'));
  }

  $form_state['redirect'] = 'admin/config/media/media_kaltura';
}

/**
 * Delete submit handler for media_kaltura_server_edit_form().
 */
function media_kaltura_server_delete_form_submit($form, &$form_state) {
  $server = (object) $form_state['values'];

  if (is_numeric($server->id)) {
    if ($count = db_delete('media_kaltura_server')->condition('id', $server->id)->execute()) {
      drupal_set_message(t('Server successfully deleted.'));
    }
  }

  $form_state['redirect'] = 'admin/config/media/media_kaltura';
}
