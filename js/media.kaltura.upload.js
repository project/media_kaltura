/**
 * @file
 * media.kaltura.upload.js
 */

(function ($) {
  'use strict';

  // Attach behaviors.
  Drupal.behaviors.mediaKalturaUpload = {
    attach: function (context, settings) {
      if (settings.mediaKalturaUpload && settings.mediaKalturaUpload.elements) {
        $.each(settings.mediaKalturaUpload.elements, function (key, info) {
          var $wrapper = $('#' + info.id);
          var $fileInput = $wrapper.find('input[name=fileData]');
          $fileInput.unbind('change', Drupal.mediaKalturaUpload.process);
          $fileInput.change(info, Drupal.mediaKalturaUpload.process);
          Drupal.mediaKalturaUpload.refreshFiles(info);
        });
      }
    },
    detach: function (context, settings) {
      if (settings.mediaKalturaUpload && settings.mediaKalturaUpload.elements) {
        $.each(settings.mediaKalturaUpload.elements, function (key, info) {
          var $wrapper = $('#' + info.id);
          var $fileInput = $wrapper.find('input[name=fileData]');
          $fileInput.unbind('change', Drupal.mediaKalturaUpload.process);
        });
      }
    }
  };

  Drupal.mediaKalturaUpload = Drupal.mediaKalturaUpload || {

      process: function (event) {
        var info = event.data;
        var $wrapper = $('#' + info.id);
        var $fileInput = $wrapper.find('input[name=fileData]');
        var $status = $wrapper.find('.media-kaltura-status');
        var files = info.files;

        if (!info.ks) {
          return;
        }

        if (!this.files.length) {
          return;
        }

        if (info.cardinality != '-1' && (this.files.length + files.length) > info.cardinality) {
          alert('This field only allows a maximim of ' + info.cardinality + ' files.');
          return;
        }

        // Stop page unload if there is a upload in process.
        window.onbeforeunload = function () {
          return 'You are still in the process of uploading, are you sure you want to leave this page?';
        };

        // Process files.
        $fileInput.attr('disabled', 'disabled');
        var totalFiles = this.files.length;
        $(this.files).each(function (key, file) {

          var formData = new FormData();
          var request = new XMLHttpRequest();

          formData.append('ks', info.ks + '');
          formData.append('service', 'uploadToken');
          formData.append('action', 'add');
          formData.append('format', 1);
          formData.append('uploadToken:fileName', file.name);
          formData.append('uploadToken:fileSize', file.size);

          request.responseType = 'json';
          request.open('POST', info.apiUrl, true);
          request.onload = function (evt) {
            if (evt.currentTarget.status === 200 && evt.currentTarget.response !== null) {
              var $progress = $('<div class="progress"><div class="bar"><div class="filled"></div></div><div class="percentage"></div><div class="message"></div></div>');
              var formData = new FormData();
              var request = new XMLHttpRequest();

              $status.append($progress);

              formData.append('service', 'uploadToken');
              formData.append('action', 'upload');
              formData.append('format', 1);
              formData.append('ks', info.ks);
              formData.append('uploadTokenId', evt.currentTarget.response.id);
              formData.append('fileData', file, evt.currentTarget.response.file);

              request.responseType = 'json';
              request.open('POST', info.apiUrl, true);
              request.onload = function (evt) {
                if (evt.currentTarget.status === 200 && evt.currentTarget.response !== null && evt.currentTarget.response.id) {
                  var formData = new FormData();
                  var request = new XMLHttpRequest();

                  formData.append('mimetype', file.type);
                  formData.append('uploadTokenId', evt.currentTarget.response.id);

                  request.responseType = 'json';
                  request.open('POST', Drupal.settings.basePath + 'media_kaltura/upload/', true);
                  request.onload = function (evt) {
                    if (evt.currentTarget.status === 200 && evt.currentTarget.response !== null && evt.currentTarget.response.fid) {
                      $progress.remove();
                      files.push(evt.currentTarget.response);
                      Drupal.mediaKalturaUpload.refreshFiles(info);
                      if (totalFiles === (key + 1)) {
                        $fileInput.attr('disabled', '');
                        window.onbeforeunload = null;
                      }
                    }
                    else {
                      alert(evt.currentTarget.statusText);
                    }
                  };
                  request.send(formData);
                }
              };
              request.upload.addEventListener('progress', function (evt) {
                if (evt.lengthComputable) {
                  var percentComplete = Math.round(evt.loaded / evt.total * 100);
                  var message = 'Uploading <em>' + file.name + '</em>... Please wait.';
                  $progress.find('.filled').width(percentComplete + '%');
                  $progress.find('.percentage').text(percentComplete + '%');
                  $progress.find('.message').html(message);
                }
              }, false);
              request.send(formData);
            }
            else {
              alert(evt.currentTarget.statusText);
            }
          };
          request.onerror = function (evt) {
            alert(evt.currentTarget.statusText);
          };
          request.send(formData);
        });
      },

      refreshFiles: function (info) {
        var $wrapper = $('#' + info.id);
        var $fileInput = $wrapper.find('input[name=fileData]');
        var $files = $wrapper.find('.media-kaltura-files');
        var $preview = $wrapper.find('.media-kaltura-preview');
        var files = info.files;

        $preview.show();
        $preview.empty();
        $files.empty();
        if (files.length) {
          var $table = $('<table><thead><tr><th>File information</th><th>Operations</th></tr></thead></table>');
          var $tbody = $('<tbody></tbody>');
          files.forEach(function (file, delta) {
            $tbody.append('<tr><td>' + file.filename + '</td><td><a href="' + Drupal.settings.basePath + 'file/' + file.fid + '/edit" target="_blank">edit</a></td></tr>');
            $files.append('<input class="files-input" type="hidden" name="' + info.name + '[' + delta + '][fid]" value="' + file.fid + '">');
          });
          $table.append($tbody);
          $preview.append($table);
        }
        else {
          $preview.hide();
        }

        if (info.cardinality != '-1' && files.length >= info.cardinality) {
          $fileInput.hide();
        }
        else {
          $fileInput.show();
        }
      }
    };

})(jQuery);
