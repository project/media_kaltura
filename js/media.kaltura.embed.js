/**
 * @file
 * media.kaltura.embed.js
 */

(function ($) {
  'use strict';

  // Create an object to place public functions.
  Drupal.mediaKaltura = Drupal.mediaKaltura || {};

  // Attach behaviors.
  Drupal.behaviors.mediaKalturaEmbed = {
    attach: function (context, settings) {
      Drupal.mediaKaltura.kWidgetStatus();
    }
  };

  // Embed Kaltura media using the kWidget API.
  Drupal.mediaKaltura.embedPlayers = function () {
    var $players = $('.media-kaltura-wrapper.media-kaltura-responsive:not(.media-kaltura-processed), .media-kaltura-wrapper.media-kaltura-dynamic:not(.media-kaltura-processed), .media-kaltura-wrapper.media-kaltura-thumbnail:not(.media-kaltura-processed)');
    $players.each(function (key, element) {
      var $wrapper = $(element);
      var $player = $wrapper.find('.media-kaltura-player');
      var conf = $player.data('conf');

      // Lead with HTML5 if supported.
      if (window.kWidget.supportsHTML5()) {
        window.mw.setConfig('KalturaSupport.LeadWithHTML5', true);
      }

      // Embed the video.
      if (conf.embedType === 'thumbnail') {
        $player.width(conf.width).height(conf.height);
        window.kWidget.thumbEmbed(conf);
      }
      else if (conf.embedType === 'responsive') {
        $wrapper.css({
          'width': '100%',
          'display': 'inline-block',
          'position': 'relative',
          'max-width': conf.width,
          'max-height': conf.height
        });
        $wrapper.find('.media-kaltura-aspect').css({
          'margin-top': '56.25%'
        });
        $player.css({
          'position': 'absolute',
          'top': '0',
          'left': '0',
          'right': '0',
          'bottom': '0'
        });
        delete conf.width;
        delete conf.height;
        window.kWidget.embed(conf);
      }
      else {
        window.kWidget.embed(conf);
      }

      $wrapper.addClass('media-kaltura-processed');
    });
  };

  // Continually check for kWidget until available.
  Drupal.mediaKaltura.kWidgetStatus = function () {
    if (typeof (window.kWidget) === 'undefined') {
      setTimeout(function () {
        if (typeof (window.kWidget) === 'undefined') {
          Drupal.mediaKaltura.kWidgetStatus();
        }
        else {
          Drupal.mediaKaltura.embedPlayers();
        }
      }, 50);
    }
    else {
      Drupal.mediaKaltura.embedPlayers();
    }
  };
})(jQuery);
