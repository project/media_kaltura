<?php

/**
 * @file
 * Definition of MediaKalturaStreamWrapper.
 */

/**
 * Extends the MediaReadOnlyStreamWrapper class.
 */
class MediaKalturaStreamWrapper extends MediaReadOnlyStreamWrapper {

  /**
   * Implements MediaReadOnlyStreamWrapper::getExternalUrl().
   */
  public function getExternalUrl() {
    $parameters = $this->parameters;
    $parse_url = parse_url($GLOBALS['base_url']);
    $scheme = $parse_url['scheme'];
    $url = $scheme . '://' . $parameters['domain'] . '/p/' . $parameters['partner_id'] . '/sp/' . $parameters['subpartner_id'] . '/raw/entry_id/' . $parameters['entry_id'];

    // Allow modules to alter external url.
    drupal_alter('media_kaltura_url', $url, $parameters, $scheme);

    return $url;
  }

  /**
   * Implements MediaReadOnlyStreamWrapper::setUri().
   */
  public function setUri($uri) {
    $path = explode('://', $uri);
    $parameters = explode('/', $path[1]);

    if (count($parameters) > 5) {
      $parameters = array_slice($parameters, -5);
    }

    $this->uri = $path[0] . '://' . implode('/', $parameters);
    $this->parameters = array(
      'domain' => $parameters[0],
      'partner_id' => $parameters[1],
      'subpartner_id' => $parameters[2],
      'uiconf_id' => $parameters[3],
      'entry_id' => $parameters[4],
    );
  }

  /**
   * Implements MediaReadOnlyStreamWrapper::getMimeType().
   */
  public static function getMimeType($uri, $mapping = NULL) {

    // Attempt to get mimetype using Kaltura Client.
    try {
      $path = explode('://', $uri);
      $parts = explode('/', $path[1]);
      $parameters = array(
        'domain' => $parts[0],
        'partner_id' => $parts[1],
        'subpartner_id' => $parts[2],
        'uiconf_id' => $parts[3],
        'entry_id' => $parts[4],
      );

      // Get server id if available.
      $id = db_query('SELECT id FROM {media_kaltura_server} WHERE partner_id = :partner_id AND subpartner_id = :subpartner_id', array(':partner_id' => $parameters['partner_id'], ':subpartner_id' => $parameters['subpartner_id']))->fetchField();
      if (!$id) {
        throw new Exception('No server configured for this file.');
      }

      // Check that server exists.
      $server = media_kaltura_server_load($id);
      if (!$server) {
        throw new Exception('Unable to load Kaltura server.');
      }

      // Start a new session with the Kaltura server.
      $kaltura = media_kaltura_start_session($server);
      if (!$kaltura) {
        throw new Exception('Unable to start Kaltura session.');
      }

      $entry = $kaltura['client']->media->get($parameters['entry_id']);

      if ($entry->mediaType) {
        switch ($entry->mediaType) {
          case KalturaMediaType::AUDIO:
            return 'audio/kaltura';

          case KalturaMediaType::IMAGE:
            return 'image/kaltura';

          case KalturaMediaType::VIDEO:
            return 'video/kaltura';
        }
      }
    }
    catch (Exception $e) {
      watchdog('media_kaltura', 'There was a problem fetching the kaltura media entry for @uri: @error', array('@uri' => $uri, '@error' => $e->getMessage()));
    }

    // Check and see if there is already a file_managed entry in case the
    // Kaltura server is down.
    try {
      $type = db_select('file_managed', 'f')
        ->fields('f', array('type'))
        ->condition('uri', $uri)
        ->execute()
        ->fetchField();

      if (in_array($type, array('audio', 'image', 'video'))) {
        return $type . '/kaltura';
      }
    }
    catch (Exception $e) {
      watchdog('media_kaltura', 'There was a problem fetching the kaltura media entry for @uri: @error', array('@uri' => $uri, '@error' => $e->getMessage()));
    }

    return 'video/kaltura';
  }
}
