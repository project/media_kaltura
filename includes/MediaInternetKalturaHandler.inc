<?php

/**
 * @file
 * Definition of MediaInternetKalturaHandler.
 */

/**
 * Extends the MediaInternetBaseHandler class.
 */
class MediaInternetKalturaHandler extends MediaInternetBaseHandler {

  /**
   * Helper function for parsing a kaltura embed code.
   *
   * @return string
   *   A string of the file uri.
   */
  public function parse($embed_code) {
    return media_kaltura_media_parse($embed_code);
  }

  /**
   * Implements MediaInternetBaseHandler::claim().
   */
  public function claim($embed_code) {
    return is_string($this->parse($embed_code));
  }

  /**
   * Implements MediaInternetBaseHandler::save().
   */
  public function save() {
    $file = $this->getFileObject();
    // If a user enters a duplicate object, the object will be saved again.
    // Set the timestamp to the current time, so that the media item shows up
    // at the top of the media library, where they would expect to see it.
    $file->timestamp = REQUEST_TIME;
    file_save($file);
    return $file;
  }

  /**
   * Implements MediaInternetBaseHandler::getFileObject().
   */
  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    return file_uri_to_object($uri, TRUE);
  }

}
