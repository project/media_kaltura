<?php

/**
 * @file
 * Definition of MediaKalturaBrowser.
 */

/**
 * Media browser plugin for Media Kaltura.
 */
class MediaKalturaBrowser extends MediaBrowserPlugin {

  /**
   * Implements MediaBrowserPluginInterface::access().
   */
  public function access($account = NULL) {
    return file_entity_access('create');
  }

  /**
   * Implements MediaBrowserPlugin::view().
   */
  public function view() {
    $build['form'] = drupal_get_form('media_kaltura_add', $this->params['types'], $this->params['multiselect'], TRUE);
    return $build;
  }

}
