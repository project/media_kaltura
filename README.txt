INTRODUCTION
------------
The Media: Kaltura module adds support for embedding videos from Kaltura
media servers. It also provides support for using the Kaltura Client API
to upload, update, and delete media on authorized Kaltura servers.

REQUIREMENTS
------------
 * File Entity (>=2.x) - https://drupal.org/project/file_entity
 * Media (>=2.x) - https://drupal.org/project/media
 * Libraries (>=2.x) - https://drupal.org/project/libraries
 * Kaltura Client PHP5 API - http://www.kaltura.com/api_v3/testme/client-libs.php

INSTALLATION
------------
** Use the drush command 'drush kcdl' to automatically download the libraries.

1. Install the Kaltura Client API library in sites/{site_name}/libraries. KalturaClient.php
   should be located at sites/{site_name}/libraries/KalturaClient/KalturaClient.php.

2. Install dependencies and media_kaltura module as per:
   https://drupal.org/documentation/install/modules-themes/modules-7

3. (Optional) Install the jQuery Update module and use a 1.9+ version. This will
   allow WYSIWYG inserts to work properly with non-iframe Kaltura displays.

4. Visit the Media: Kaltura configuration page to set server options,
   etc, at admin/config/media/media_kaltura.
