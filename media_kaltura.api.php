<?php

/**
 * @file
 * API documentation for the Media: Kaltura module.
 */

/**
 * Alter the media entry data before it is created or updated.
 *
 * This is called before the Kaltura media entry has been created or updated, so
 * the entry ID is not yet available.
 *
 * @param object $entry
 *   A kaltura media entry object.
 * @param object $file
 *   A drupal file object.
 * @param object $client
 *   The Kaltura Client object.
 */
function hook_media_kaltura_entry_alter(&$entry, $file, $client) {
  $entry->categories = 'Samples>Sample Category';
  $entry->categoriesID = '0000001';
}

/**
 * Act on the creation or update of a Kaltura media entry.
 *
 * This is called after the Kaltura media entry has been created or updated, so
 * that the entry ID is available.
 *
 * @param object $entry
 *   A kaltura media entry object.
 * @param object $file
 *   A drupal file object.
 * @param object $client
 *   The Kaltura Client object.
 */
function hook_media_kaltura_entry($entry, $file, $client) {
  $categoryEntry = new KalturaCategoryEntry();
  $categoryEntry->categoryId = 0000001;
  $categoryEntry->entryId = $entry->id;
  $client->categoryEntry->add($categoryEntry);
}
