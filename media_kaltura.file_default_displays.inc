<?php

/**
 * @file
 * Media: Kaltura display configuration for the default file types.
 */

/**
 * Implements hook_file_default_displays().
 */
function media_kaltura_file_default_displays() {
  $file_displays = array();

  // Audio: Default display.
  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__default__media_kaltura_audio';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'embed_method' => 'auto',
    'session' => 0,
    'title' => 0,
    'logo' => 0,
    'width' => '300',
    'height' => '80',
    'autoplay' => FALSE,
  );
  $file_displays['audio__default__media_kaltura_audio'] = $file_display;

  // Audio: Teaser display.
  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__teaser__media_kaltura_audio';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'embed_method' => 'thumbnail',
    'session' => 0,
    'title' => 0,
    'logo' => 0,
    'width' => '300',
    'height' => '80',
    'autoplay' => FALSE,
  );
  $file_displays['audio__teaser__media_kaltura_audio'] = $file_display;

  // Audio: Preview display.
  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__preview__file_field_media_large_icon';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'media_thumbnail',
  );
  $file_displays['audio__preview__file_field_media_large_icon'] = $file_display;

  // Image: Default display.
  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__default__media_kaltura_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '',
    'height' => '',
  );
  $file_displays['image__default__media_kaltura_image'] = $file_display;

  // Image: Teaser display.
  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__teaser__media_kaltura_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '',
    'height' => '',
  );
  $file_displays['image__teaser__media_kaltura_image'] = $file_display;

  // Image: Preview display.
  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__preview__file_field_media_large_icon';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'media_thumbnail',
  );
  $file_displays['image__preview__file_field_media_large_icon'] = $file_display;

  // Video: Default display.
  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__default__media_kaltura_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'embed_method' => 'responsive',
    'session' => 0,
    'title' => 0,
    'logo' => 0,
    'width' => '640',
    'height' => '360',
    'autoplay' => FALSE,
  );
  $file_displays['video__default__media_kaltura_video'] = $file_display;

  // Video: Teaser display.
  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__teaser__media_kaltura_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'embed_method' => 'thumbnail',
    'session' => 0,
    'title' => 0,
    'logo' => 0,
    'width' => '240',
    'height' => '180',
    'autoplay' => FALSE,
  );
  $file_displays['video__teaser__media_kaltura_video'] = $file_display;

  // Video: Preview display.
  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__preview__file_field_media_large_icon';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'media_thumbnail',
  );
  $file_displays['video__preview__file_field_media_large_icon'] = $file_display;

  return $file_displays;
}
